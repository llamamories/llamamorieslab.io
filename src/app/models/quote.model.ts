import Author from './author.model';

export default interface Quote {
	id?: String;
	text: String;
	author: Author;
}
