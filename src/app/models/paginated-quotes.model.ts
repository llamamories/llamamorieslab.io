import Quote from './quote.model';

export default interface PaginatedQuotes {
	page: number;
	pages: number;
	quotes: Quote[];
	totalCount: number;
}
