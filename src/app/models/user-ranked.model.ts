export interface UserRanked {
	user: string;
	count: number;
}
