export interface AppStat {
	authorCount: number;
	quoteCount: number;
	userCount: number;
}
