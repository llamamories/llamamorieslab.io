import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { AbstractControl, UntypedFormBuilder, UntypedFormGroup, ReactiveFormsModule, ValidatorFn } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import Author from '@llama/models/author.model';
import { AuthorsRestService } from '@llama/rest/authors.rest.service';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
	selector: 'llama-search-dialog',
	templateUrl: './search-dialog.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
	standalone: true,
	imports: [
		CommonModule,
		MatAutocompleteModule,
		MatButtonModule,
		MatDialogModule,
		MatFormFieldModule,
		MatInputModule,
		MatSnackBarModule,
		ReactiveFormsModule,
	],
})
export class SearchDialogComponent implements OnInit {
	searchForm: UntypedFormGroup;
	filteredOptions: Observable<Author[]>;

	submitted: boolean;

	private options: Author[];

	constructor(
		private dialogRef: MatDialogRef<SearchDialogComponent>,
		private fb: UntypedFormBuilder,
		private authorsService: AuthorsRestService,
		private router: Router,
	) {}

	ngOnInit() {
		this.searchForm = this.fb.group({
			author: [{ value: null, disabled: true }, this.authorExists()],
			term: [null],
		});

		this.authorsService.getAll().subscribe(authors => {
			this.options = authors;
			this.f.author.enable();
			this.filteredOptions = this.f.author.valueChanges.pipe(
				startWith<string | Author>(''),
				map(value => (typeof value === 'string' ? value : value.name)),
				map(name => (name ? this.filter(name) : this.options.slice())),
			);
		});
	}

	get f() {
		return this.searchForm.controls;
	}

	onSubmit() {
		if (this.searchForm.invalid || (!this.f.author.value && !this.f.term.value)) {
			return;
		}

		const author = this.f.author.value ? this.f.author.value.id : null;
		const term = this.f.term.value;

		this.submitted = true;
		this.router.navigate(['/search'], { queryParams: { author, term } }).then(() => this.dialogRef.close());
	}

	display(author?: Author): string | undefined {
		return author ? author.name : undefined;
	}

	private filter(name: string): Author[] {
		const filterValue = name.toLocaleLowerCase();

		return this.options.filter(option => option.name.toLocaleLowerCase().includes(filterValue));
	}

	private authorExists(): ValidatorFn {
		return (control: AbstractControl): { [key: string]: any } | null => {
			if (!control || !control.value) {
				return null;
			}
			return this.options.includes(control.value)
				? null
				: {
						inexistantAuthor: true,
				  };
		};
	}
}
