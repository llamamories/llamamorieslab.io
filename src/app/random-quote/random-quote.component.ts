import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import Quote from '@llama/models/quote.model';
import { QuotePageComponent } from '@llama/quote-page/quote-page.component';
import { RandomQuoteButtonComponent } from '@llama/random-quote-button/random-quote-button.component';
import { QuotesRestService } from '@llama/rest/quotes.rest.service';
import { Observable } from 'rxjs';

@Component({
	selector: 'llama-random-quote',
	templateUrl: './random-quote.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
	standalone: true,
	imports: [MatButtonModule, QuotePageComponent, RandomQuoteButtonComponent],
})
export class RandomQuoteComponent implements OnInit {
	quote$: Observable<Quote>;

	constructor(private quoteService: QuotesRestService) {}

	ngOnInit() {
		this.getRandomQuote();
	}

	getRandomQuote() {
		this.quote$ = this.quoteService.getRandom();
	}
}
