import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SwUpdate } from '@angular/service-worker';

@Injectable({
	providedIn: 'root',
})
export class UpdateService {
	constructor(private swUpdate: SwUpdate, private snackbar: MatSnackBar) {
		// Check if there is an update available and reload the page if wanted by the user.
		this.swUpdate.versionUpdates.subscribe(_ => {
			const snack = this.snackbar.open('Mise à jour disponible', 'Recharger', {
				duration: 5000,
				panelClass: ['snackbar-info'],
			});

			snack.onAction().subscribe(() => location.reload());
		});
	}
}
