import { AfterViewInit, Component, ElementRef, OnDestroy, Renderer2, ViewChild } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FabMenuComponent } from './fab-menu/fab-menu.component';

type Season = 'none' | 'xmas' | 'bday';

@Component({
	selector: 'llama-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.sass'],
	standalone: true,
	imports: [FabMenuComponent, RouterModule],
})
export class AppComponent implements AfterViewInit, OnDestroy {
	@ViewChild('scrollContainer') container: ElementRef;
	@ViewChild('logo') logo: ElementRef;

	private listener: () => void;
	private logoHeight: number;

	readonly llama: Record<Season, string> = {
		none: 'assets/llama.webp',
		xmas: 'assets/llama-xmas.webp',
		bday: 'assets/llama-bday.webp',
	} as const;

	season: Season = this.getSeason();

	constructor(private renderer: Renderer2) {}

	ngAfterViewInit() {
		this.logoHeight = this.logo.nativeElement.offsetHeight;
		this.listener = this.renderer.listen(this.container.nativeElement, 'scroll', _ => {
			this.handleScroll(this.container.nativeElement.scrollTop);
		});
	}

	ngOnDestroy() {
		this.listener();
	}

	private handleScroll(scrollTop: number) {
		let height = this.logoHeight - scrollTop;
		if (height > 300) {
			height = 300;
		} else if (height < 96) {
			height = 96;
		}
		this.renderer.setStyle(this.logo.nativeElement, 'height', `${height}px`);
		this.renderer.setStyle(this.logo.nativeElement, 'width', `${height / 2}px`);
	}

	private getSeason(): Season {
		const now = new Date();

		if (now.getMonth() === 11) {
			return 'xmas';
		}

		// The llama's bday is on the 26/01, let's celebrate it for 1 week before and 1 week after
		if ((now.getMonth() === 0 && now.getDate() >= 19) || (now.getMonth() === 1 && now.getDate() <= 2)) {
			return 'bday';
		}

		return 'none';
	}
}
