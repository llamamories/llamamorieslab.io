import { ChangeDetectionStrategy, Component, computed, inject, input, Signal } from '@angular/core';
import Quote from '@llama/models/quote.model';
import { QuotePageComponent } from '@llama/quote-page/quote-page.component';
import { QuotesRestService } from '@llama/rest/quotes.rest.service';
import { Observable } from 'rxjs';

@Component({
	selector: 'llama-quote-by-id',
	templateUrl: './quote-by-id.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
	standalone: true,
	imports: [QuotePageComponent],
})
export class QuoteByIdComponent {
	quoteId = input.required<string>();

	private quotesApi = inject(QuotesRestService);

	quote$: Signal<Observable<Quote>> = computed(() => this.quotesApi.getById(this.quoteId()));
}
