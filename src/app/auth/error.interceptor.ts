import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UsersRestService } from '@llama/rest/users.rest.service';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LoginDialogComponent } from './login-dialog/login-dialog.component';

@Injectable({ providedIn: 'root' })
export class ErrorInterceptor implements HttpInterceptor {
	usersRestService = inject(UsersRestService);
	snackBar = inject(MatSnackBar);
	dialog = inject(MatDialog);

	intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		return next.handle(request).pipe(
			catchError(err => {
				if (err.status === 401) {
					// auto logout if 401 response returned from api
					this.usersRestService.logout();
					if (this.usersRestService.currentUserValue) {
						const snackbar = this.snackBar.open(`Erreur de connexion, merci de te reconnecter :)`, 'login', {
							duration: 10_000,
							panelClass: ['snackbar-error'],
						});

						snackbar.onAction().subscribe(() =>
							this.dialog.open(LoginDialogComponent, {
								panelClass: 'dialog-panel',
							}),
						);
					}
				}

				const error = err.error.message || err.statusText;

				return throwError(() => error);
			}),
		);
	}
}
