
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { UsersRestService } from '@llama/rest/users.rest.service';

@Component({
	selector: 'llama-create-user-dialog',
	templateUrl: './create-user-dialog.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
	standalone: true,
	imports: [
    MatAutocompleteModule,
    MatButtonModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
    ReactiveFormsModule
],
})
export class CreateUserDialogComponent implements OnInit {
	createForm: UntypedFormGroup;
	loading: boolean;
	submitted: boolean;

	constructor(
		private dialogRef: MatDialogRef<CreateUserDialogComponent>,
		private fb: UntypedFormBuilder,
		private usersService: UsersRestService,
		private snackBar: MatSnackBar,
	) {}

	ngOnInit() {
		this.createForm = this.fb.group({
			username: [null, Validators.required],
		});
	}

	get f() {
		return this.createForm.controls;
	}

	onSubmit() {
		this.submitted = true;
		if (this.createForm.invalid) {
			return;
		}

		this.loading = true;
		this.usersService.createUser(this.f.username.value).subscribe({
			next: _ => {
				this.snackBar.open('Utilisateur bien créé 👌', null, {
					duration: 1000,
					panelClass: ['snackbar-success'],
				});
				this.loading = false;
				this.dialogRef.close();
			},
			error: () => {
				this.snackBar.open(`Nom d'utilisateur ou mot de passe incorrect.`, null, {
					duration: 1000,
					panelClass: ['snackbar-error'],
				});
				this.loading = false;
				this.dialogRef.close();
			},
		});
	}

	onCancel() {
		this.dialogRef.close();
	}
}
