
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { UsersRestService } from '@llama/rest/users.rest.service';

@Component({
	selector: 'llama-login-dialog',
	templateUrl: './login-dialog.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
	standalone: true,
	imports: [
    MatAutocompleteModule,
    MatButtonModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
    ReactiveFormsModule
],
})
export class LoginDialogComponent implements OnInit {
	loginForm: UntypedFormGroup;
	loading: boolean;
	submitted: boolean;

	constructor(
		private dialogRef: MatDialogRef<LoginDialogComponent>,
		private fb: UntypedFormBuilder,
		private usersService: UsersRestService,
		private snackBar: MatSnackBar,
	) {}

	ngOnInit() {
		this.loginForm = this.fb.group({
			username: [null, Validators.required],
			password: [null, Validators.required],
		});
	}

	get f() {
		return this.loginForm.controls;
	}

	onSubmit() {
		this.submitted = true;
		if (this.loginForm.invalid) {
			return;
		}

		this.loading = true;
		this.usersService.login(this.f.username.value, this.f.password.value).subscribe({
			next: _ => {
				this.snackBar.open('Bienvenue cher lama !', null, {
					duration: 1000,
					panelClass: ['snackbar-success'],
				});
				this.dialogRef.close();
			},
			error: () => {
				this.snackBar.open(`Nom d'utilisateur ou mot de passe incorrect.`, null, {
					duration: 1000,
					panelClass: ['snackbar-error'],
				});
				this.loading = false;
			},
		});
	}

	onCancel() {
		this.dialogRef.close();
	}
}
