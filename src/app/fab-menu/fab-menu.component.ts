import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDialog, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { RouterModule } from '@angular/router';
import { CreateUserDialogComponent } from '@llama/auth/create-user-dialog/create-user-dialog.component';
import { LoginDialogComponent } from '@llama/auth/login-dialog/login-dialog.component';
import { CreateQuoteDialogComponent } from '@llama/create-quote-dialog/create-quote-dialog.component';
import { UsersRestService } from '@llama/rest/users.rest.service';
import { SearchDialogComponent } from '@llama/search-dialog/search-dialog.component';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import { FabMenuAnimations } from './fab-menu.animations';
import MenuItem from './menu-item';

@Component({
	selector: 'llama-fab-menu',
	templateUrl: './fab-menu.component.html',
	styleUrls: ['./fab-menu.component.sass'],
	animations: [FabMenuAnimations],
	changeDetection: ChangeDetectionStrategy.OnPush,
	standalone: true,
	imports: [MatButtonModule, MatDialogModule, MatIconModule, MatMenuModule, RouterModule, SearchDialogComponent],
})
export class FabMenuComponent implements OnInit, OnDestroy {
	private readonly all: MenuItem = {
		icon: 'list',
		color: 'primary',
		url: '/all',
		command: () => this.hide(),
		title: 'Voir toutes les citations',
	};

	private readonly authenticate: MenuItem = {
		icon: 'key',
		color: 'primary',
		command: () => this.showLoginDialog(),
		title: 'Se connecter',
	};

	private readonly create: MenuItem = {
		icon: 'add',
		color: 'primary',
		command: () => this.showCreateDialog(),
		title: 'Ajouter une citation',
	};

	private readonly exit: MenuItem = {
		icon: 'exit_to_app',
		color: 'warn',
		command: () => this.logout(),
		title: 'Se déconnecter',
	};

	private readonly stats: MenuItem = {
		icon: 'trending_up',
		color: 'primary',
		url: '/stats',
		command: () => this.hide(),
		title: 'Statistiques',
	};

	private readonly search: MenuItem = {
		icon: 'search',
		color: 'primary',
		command: () => this.showSearchDialog(),
		title: 'Chercher une citation',
	};

	private readonly createUser: MenuItem = {
		icon: 'person_add',
		color: 'primary',
		command: () => this.showCreateUserDialog(),
		title: 'Ajouter un utilisateur',
	};

	buttons: MenuItem[] = [];
	fabState: 'active' | 'inactive' = 'inactive';

	loginDialog: MatDialogRef<LoginDialogComponent>;
	createUserDialog: MatDialogRef<CreateUserDialogComponent>;
	createDialog: MatDialogRef<CreateQuoteDialogComponent>;
	searchDialog: MatDialogRef<SearchDialogComponent>;

	private items: MenuItem[];
	private subscription: Subscription;
	private isAdmin: boolean;
	constructor(private usersService: UsersRestService, private dialog: MatDialog, private cdRef: ChangeDetectorRef) {}

	ngOnInit() {
		this.items = this.usersService.currentUserValue ? this.loggedInItems : this.loggedOutItems;
		this.subscription = this.usersService.currentUser.subscribe(currentUser => {
			this.isAdmin = currentUser ? currentUser.admin : false;
			this.items = currentUser ? this.loggedInItems : this.loggedOutItems;
		});
	}

	ngOnDestroy() {
		this.subscription.unsubscribe();
	}

	private get loggedInItems(): MenuItem[] {
		let items = [this.create, this.search, this.all, this.stats];

		if (this.isAdmin) {
			items = [...items, this.createUser];
		}

		return [...items, this.exit];
	}

	private get loggedOutItems(): MenuItem[] {
		return [this.search, this.all, this.stats, this.authenticate];
	}

	onToggle() {
		this.buttons.length ? this.hide() : this.show();
	}

	private show() {
		this.fabState = 'active';
		this.buttons = this.items;
	}

	private hide() {
		this.fabState = 'inactive';
		this.buttons = [];
		this.cdRef.detectChanges();
	}

	private showLoginDialog() {
		this.loginDialog = this.dialog.open(LoginDialogComponent, {
			panelClass: 'dialog-panel',
		});

		this.loginDialog
			.afterOpened()
			.pipe(first())
			.subscribe(() => this.hide());
	}

	private showCreateDialog() {
		this.createDialog = this.dialog.open(CreateQuoteDialogComponent, {
			panelClass: 'dialog-panel',
		});

		this.createDialog
			.afterOpened()
			.pipe(first())
			.subscribe(() => this.hide());
	}

	private showSearchDialog() {
		this.searchDialog = this.dialog.open(SearchDialogComponent, {
			panelClass: 'dialog-panel',
		});

		this.searchDialog
			.afterOpened()
			.pipe(first())
			.subscribe(() => this.hide());
	}

	private showCreateUserDialog() {
		this.createUserDialog = this.dialog.open(CreateUserDialogComponent, {
			panelClass: 'dialog-panel',
		});

		this.createUserDialog
			.afterOpened()
			.pipe(first())
			.subscribe(() => this.hide());
	}

	private logout() {
		this.usersService.logout();
		this.hide();
	}
}
