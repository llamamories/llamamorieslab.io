export default interface MenuItem {
	icon: string;
	color: 'primary' | 'accent' | 'warn';
	disabled?: boolean;
	command?: () => void;
	url?: string;
	title?: string;
}
