
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';

@Component({
	selector: 'llama-download-app',
	templateUrl: './download-app.component.html',
	styleUrls: ['./download-app.component.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush,
	standalone: true,
	imports: [MatButtonModule, MatIconModule, MatMenuModule],
})
export class DownloadAppComponent {}
