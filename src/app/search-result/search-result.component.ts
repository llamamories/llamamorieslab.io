import { ChangeDetectionStrategy, ChangeDetectorRef, Component, effect, input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import PaginatedQuotes from '@llama/models/paginated-quotes.model';
import Quote from '@llama/models/quote.model';
import { QuotePageComponent } from '@llama/quote-page/quote-page.component';
import { QuoteComponent } from '@llama/quote/quote.component';
import { QuotesRestService } from '@llama/rest/quotes.rest.service';
import { Observable, of, Subscription } from 'rxjs';

@Component({
	selector: 'llama-search-result',
	templateUrl: './search-result.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
	standalone: true,
	imports: [QuoteComponent, QuotePageComponent],
})
export class SearchResultComponent {
	author = input<string>();
	term = input<string>();

	list: Quote[] = [];
	loading: boolean = true;

	noResultQuote$: Observable<Quote> = of({
		text: `Ceci n'est pas une citation. (Pas de résultat)`,
		author: {
			name: 'Le lama',
		},
	});

	private latestData: PaginatedQuotes;

	private searchSubscription: Subscription;

	constructor(private quotesService: QuotesRestService, private cdRef: ChangeDetectorRef) {
		effect(() => this.search(true));
	}

	private get allFetched(): boolean {
		return this.latestData && this.latestData.page === this.latestData.pages;
	}

	search(reset: boolean = false) {
		if (!this.author() && !this.term()) {
			return;
		}

		if (reset) {
			delete this.latestData;
			this.list = [];
		}

		if (this.allFetched) {
			return;
		}

		// Avoid multiple requests
		this.searchSubscription?.unsubscribe();

		this.loading = true;
		const page = this.latestData ? this.latestData.page + 1 : 1;
		this.searchSubscription = this.quotesService.search(this.author(), this.term(), page).subscribe(data => {
			this.latestData = data;
			this.list = [...this.list, ...data.quotes];
			this.loading = false;
			this.cdRef.detectChanges();
		});
	}
}
