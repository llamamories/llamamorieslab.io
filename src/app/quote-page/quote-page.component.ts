import { ChangeDetectionStrategy, Component, EventEmitter, input, Output } from '@angular/core';
import Quote from '@llama/models/quote.model';
import { QuoteComponent } from '@llama/quote/quote.component';
import { InfiniteScrollDirective } from 'ngx-infinite-scroll';
import { Observable } from 'rxjs';

@Component({
	selector: 'llama-quote-page',
	templateUrl: './quote-page.component.html',
	styleUrls: ['./quote-page.component.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush,
	standalone: true,
	imports: [InfiniteScrollDirective, QuoteComponent],
})
export class QuotePageComponent {
	quoteObservable = input<Observable<Quote>>();

	quotes = input<Quote[]>();

	@Output()
	loadQuotes = new EventEmitter();
}
