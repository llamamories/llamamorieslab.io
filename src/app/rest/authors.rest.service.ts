import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import Author from '@llama/models/author.model';
import { Observable } from 'rxjs';
import RestClient from './rest.client';

@Injectable({
	providedIn: 'root',
})
export class AuthorsRestService {
	private restClient: RestClient;

	constructor(http: HttpClient) {
		this.restClient = new RestClient(http, 'authors');
	}

	getAll(): Observable<Author[]> {
		return this.restClient.get<Author[]>();
	}
}
