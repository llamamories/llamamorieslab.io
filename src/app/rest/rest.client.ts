import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

export default class RestClient {
	private readonly endpoint = 'https://llamamories-gc.ew.r.appspot.com';

	constructor(private http: HttpClient, private path: string) {}

	private get basePath(): string {
		return `${this.endpoint}/${this.path}`;
	}

	get<T>(path: string = ''): Observable<T> {
		return this.http.get<T>(`${this.basePath}/${path}`);
	}

	post<T>(body: any, path: string = ''): Observable<T> {
		return this.http.post<T>(`${this.basePath}/${path}`, body);
	}
}
