import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppStat } from '@llama/models/app-stat.model';
import { UserRanked } from '@llama/models/user-ranked.model';
import { Observable } from 'rxjs';
import RestClient from './rest.client';

@Injectable({ providedIn: 'root' })
export class StatsRestService {
	private restClient: RestClient;

	constructor(http: HttpClient) {
		this.restClient = new RestClient(http, 'stats');
	}

	get hallOfBlame(): Observable<UserRanked[]> {
		return this.restClient.get<UserRanked[]>('hall-of-blame');
	}

	get hallOfBlamed(): Observable<UserRanked[]> {
		return this.restClient.get<UserRanked[]>('hall-of-blamed');
	}

	get stats(): Observable<AppStat> {
		return this.restClient.get<AppStat>('app');
	}
}
