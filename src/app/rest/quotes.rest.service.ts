import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import PaginatedQuotes from '@llama/models/paginated-quotes.model';
import Quote from '@llama/models/quote.model';
import { Observable } from 'rxjs';
import RestClient from './rest.client';

@Injectable({
	providedIn: 'root',
})
export class QuotesRestService {
	private restClient: RestClient;

	constructor(http: HttpClient) {
		this.restClient = new RestClient(http, 'quotes');
	}

	getById(id: string): Observable<Quote> {
		return this.restClient.get<Quote>(`${id}`);
	}

	getAll(page: number = 1, limit: number = 20): Observable<PaginatedQuotes> {
		return this.restClient.get<PaginatedQuotes>(`?page=${page}&limit=${limit}`);
	}

	getToday(): Observable<Quote> {
		return this.restClient.get<Quote>('today');
	}

	getRandom(): Observable<Quote> {
		return this.restClient.get<Quote>('random');
	}

	search(author: string, term: string, page: number = 1, limit: number = 10): Observable<PaginatedQuotes> {
		let url = `search?page=${page}&limit=${limit}`;
		url += author ? `&author=${author}` : '';
		url += term ? `&term=${term}` : '';
		return this.restClient.get<PaginatedQuotes>(url);
	}

	create(quote: Quote): Observable<Quote> {
		return this.restClient.post<Quote>(quote);
	}
}
