import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import User from '@llama/models/user.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import RestClient from './rest.client';

@Injectable({
	providedIn: 'root',
})
export class UsersRestService {
	private static readonly USER_KEY = 'current_llama';

	public currentUser: Observable<User>;

	private restClient: RestClient;
	private currentUserSubject: BehaviorSubject<User>;

	constructor(http: HttpClient) {
		this.restClient = new RestClient(http, 'users');
		this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem(UsersRestService.USER_KEY)));
		this.currentUser = this.currentUserSubject.asObservable();
	}

	get currentUserValue(): User {
		return this.currentUserSubject.value;
	}

	login(username: string, password: string) {
		return this.restClient.post<User>({ username, password }, 'authenticate').pipe(
			map(user => {
				if (user?.token) {
					localStorage.setItem(UsersRestService.USER_KEY, JSON.stringify(user));
					this.currentUserSubject.next(user);
				}
				return user;
			}),
		);
	}

	createUser(username: string) {
		return this.restClient.post<User>({ username }, 'create');
	}

	logout() {
		localStorage.removeItem(UsersRestService.USER_KEY);
		this.currentUserSubject.next(null);
	}
}
