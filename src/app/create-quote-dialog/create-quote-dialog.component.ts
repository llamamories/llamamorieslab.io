import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import Author from '@llama/models/author.model';
import Quote from '@llama/models/quote.model';
import { AuthorsRestService } from '@llama/rest/authors.rest.service';
import { QuotesRestService } from '@llama/rest/quotes.rest.service';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
	selector: 'llama-create-quote-dialog',
	templateUrl: './create-quote-dialog.component.html',
	styleUrls: ['./create-quote-dialog.component.sass'],
	standalone: true,
	imports: [
		CommonModule,
		MatAutocompleteModule,
		MatButtonModule,
		MatDialogModule,
		MatFormFieldModule,
		MatInputModule,
		MatSnackBarModule,
		ReactiveFormsModule,
	],
})
export class CreateQuoteDialogComponent implements OnInit {
	createForm: UntypedFormGroup;
	filteredOptions: Observable<Author[]>;

	submitted: boolean;
	loading: boolean;

	private options: Author[];

	constructor(
		private dialogRef: MatDialogRef<CreateQuoteDialogComponent>,
		private fb: UntypedFormBuilder,
		private authorsService: AuthorsRestService,
		private quotesService: QuotesRestService,
		private snackBar: MatSnackBar,
	) {}

	ngOnInit() {
		this.createForm = this.fb.group({
			author: [{ value: null, disabled: true }, Validators.required],
			quote: [null, [Validators.required, Validators.pattern(/[a-zA-Z0-9]{3,}/)]],
		});

		this.authorsService.getAll().subscribe(authors => {
			this.options = authors;
			this.f.author.enable();
			this.filteredOptions = this.f.author.valueChanges.pipe(
				startWith<string | Author>(''),
				map(value => (typeof value === 'string' ? value : value.name)),
				map(name => (name ? this.filter(name) : this.options.slice())),
			);
		});
	}

	get f() {
		return this.createForm.controls;
	}

	display(author?: Author): string | undefined {
		return author ? author.name : undefined;
	}

	onSubmit() {
		this.submitted = true;

		if (this.createForm.invalid) {
			return;
		}

		this.loading = true;
		const author = this.f.author.value;
		const newQuote: Quote = {
			text: this.f.quote.value,
			author:
				typeof author === 'string'
					? {
							name: author,
					  }
					: author,
		};

		this.quotesService.create(newQuote).subscribe({
			next: _ => {
				this.snackBar.open('Citation créée !', null, {
					duration: 1000,
					panelClass: ['snackbar-success'],
				});
				this.createForm.reset({
					quote: [''],
					author: [''],
				});
				this.submitted = false;
				this.loading = false;
			},
			error: () => {
				this.snackBar.open('Impossible de créer la citation...', null, {
					duration: 1000,
					panelClass: ['snackbar-error'],
				});
				this.submitted = false;
				this.loading = false;
			},
		});
	}

	cancel() {
		this.dialogRef.close();
	}

	private filter(name: string): Author[] {
		const filterValue = name.toLocaleLowerCase();

		return this.options.filter(option => option.name.toLocaleLowerCase().includes(filterValue));
	}
}
