import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';

import { QuotesRestService } from '@llama/rest/quotes.rest.service';
import PaginatedQuotes from '@llama/models/paginated-quotes.model';
import Quote from '@llama/models/quote.model';
import { QuotePageComponent } from '@llama/quote-page/quote-page.component';

@Component({
	selector: 'llama-all-quotes',
	templateUrl: './all-quotes.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
	standalone: true,
	imports: [QuotePageComponent],
})
export class AllQuotesComponent implements OnInit {
	private latestData: PaginatedQuotes;

	list: Quote[] = [];

	loading: boolean;

	constructor(private quotesService: QuotesRestService, private cdRef: ChangeDetectorRef) {}

	ngOnInit() {
		this.getQuotes();
	}

	get allFetched(): boolean {
		return this.latestData && this.latestData.page === this.latestData.pages;
	}

	getQuotes() {
		if (this.allFetched) {
			return;
		}

		const page = (this.latestData?.page || 0) + 1;
		this.loading = true;

		this.quotesService.getAll(page).subscribe(data => {
			this.latestData = data;
			this.list = [...this.list, ...data.quotes];
			this.loading = false;
			this.cdRef.detectChanges();
		});
	}
}
