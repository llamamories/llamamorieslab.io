import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AppStat } from '@llama/models/app-stat.model';
import { UserRanked } from '@llama/models/user-ranked.model';
import { StatsRestService } from '@llama/rest/stats.rest.service';
import { Observable } from 'rxjs';

@Component({
	selector: 'llama-stats',
	templateUrl: './stats.component.html',
	styleUrls: ['./stats.component.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush,
	standalone: true,
	imports: [CommonModule],
})
export class StatsComponent {
	hallOfBlame$: Observable<UserRanked[]>;
	hallOfBlamed$: Observable<UserRanked[]>;
	stats$: Observable<AppStat>;

	constructor(statsService: StatsRestService) {
		this.hallOfBlame$ = statsService.hallOfBlame;
		this.hallOfBlamed$ = statsService.hallOfBlamed;
		this.stats$ = statsService.stats;
	}
}
