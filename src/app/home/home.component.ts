
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { DownloadAppComponent } from '@llama/download-app/download-app.component';
import Quote from '@llama/models/quote.model';
import { QuotePageComponent } from '@llama/quote-page/quote-page.component';
import { RandomQuoteButtonComponent } from '@llama/random-quote-button/random-quote-button.component';
import { Observable } from 'rxjs';
import { QuotesRestService } from '../rest/quotes.rest.service';

@Component({
	selector: 'llama-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush,
	standalone: true,
	imports: [DownloadAppComponent, QuotePageComponent, RandomQuoteButtonComponent],
})
export class HomeComponent {
	quoteOfTheDay: Observable<Quote>;

	constructor(quoteRestService: QuotesRestService) {
		this.quoteOfTheDay = quoteRestService.getToday();
	}
}
