import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, input } from '@angular/core';
import { RouterModule } from '@angular/router';
import Quote from '@llama/models/quote.model';
import { Observable } from 'rxjs';

@Component({
	selector: 'llama-quote',
	templateUrl: './quote.component.html',
	styleUrls: ['./quote.component.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush,
	standalone: true,
	imports: [CommonModule, RouterModule],
})
export class QuoteComponent {
	quoteObservable = input<Observable<Quote>>();

	quote = input<Quote>();

	variant = input<'normal' | 'condensed'>('normal');
}
