import { enableProdMode, importProvidersFrom } from '@angular/core';
import { bootstrapApplication, BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Routes, provideRouter, withComponentInputBinding } from '@angular/router';
import { ServiceWorkerModule } from '@angular/service-worker';
import { AppComponent } from '@llama/app.component';

import { environment } from './environments/environment';
import { HTTP_INTERCEPTORS, provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { ErrorInterceptor } from '@llama/auth/error.interceptor';
import { JwtInterceptor } from '@llama/auth/jwt.interceptor';

if (environment.production) {
	enableProdMode();
}

const routes: Routes = [
	{
		path: '',
		pathMatch: 'full',
		loadComponent: () => import('./app/home/home.component').then(m => m.HomeComponent),
	},
	{
		path: 'random',
		loadComponent: () => import('./app/random-quote/random-quote.component').then(m => m.RandomQuoteComponent),
	},
	{
		path: 'all',
		loadComponent: () => import('./app/all-quotes/all-quotes.component').then(m => m.AllQuotesComponent),
	},
	{
		path: 'search',
		loadComponent: () => import('./app/search-result/search-result.component').then(m => m.SearchResultComponent),
	},
	{
		path: 'quote/:quoteId',
		loadComponent: () => import('./app/quote-by-id/quote-by-id.component').then(m => m.QuoteByIdComponent),
	},
	{
		path: 'stats',
		loadComponent: () => import('./app/stats/stats.component').then(m => m.StatsComponent),
	},
];

bootstrapApplication(AppComponent, {
	providers: [
		provideRouter(routes, withComponentInputBinding()),
		provideHttpClient(withInterceptorsFromDi()),
		importProvidersFrom(
			BrowserAnimationsModule,
			BrowserModule,
			ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
		),
		{ provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
		{ provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
	],
}).catch(err => console.error(err));
