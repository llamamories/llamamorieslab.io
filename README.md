![](src/assets/llama.webp)

# Llamamories

Frontend project of [Llamamories](https://llamamories.gitlab.io) built with [Angular](https://angular.io/) and [Angular Material](https://material.angular.io/).
